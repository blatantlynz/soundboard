#!/usr/bin/python


import pyaudio
import wave
import time
import sys
import subprocess



class WavFileConverter:
    def convertToWav(self,filename):
        subprocess.call(["ffmpeg","-i",filename+".mp3","-acodec","pcm_u8","-y", "-ar", "22050", filename+".wav"],shell=False)

    def isMp3(self,filename):
        parts = filename.split(".")
        if (parts[-1] == "mp3"):
            return True
        else:
            return False

    def removeSuffix(self,filename):
        return filename.split(".")[0]


class SoundBoard:
    def __init__(self):
        self.converter = WavFileConverter()
        self.sounds = {}

    def addSound(self,alias,filename):
        if (self.converter.isMp3(filename)):
            self.converter.convertToWav(removeSuffix(filename))
            wavFilename = removeSuffix(filename)+".wav"
        else:
            wavFilename = filename
        self.sounds[alias] = wavFilename

    def getSound(self,alias):
        return self.sounds[alias]

    def getSoundDevices(self):
        p = pyaudio.PyAudio()

        deviceCount = p.get_device_count()
        print "There are " + str(deviceCount) + " devices"
        for i in range(0,deviceCount):
            print "Device: {} index: {}".format(p.get_device_info_by_index(i)['name'],i)
            #print p.get_device_info_by_index(i)['maxOutputChannels']


    def playSound(self,alias):
        if alias not in  self.sounds:
            raise Error("No sound found")
        else:
            filename = self.getSound(alias)
            p = pyaudio.PyAudio()
            wf = wave.open(filename, 'rb')

            def callback(in_data, frame_count, time_info, status):
                data = wf.readframes(frame_count)
                return (data, pyaudio.paContinue)

            stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                            channels=wf.getnchannels(),
                            output_device_index=1,
                            rate=wf.getframerate(),
                            output=True,
                            stream_callback=callback)

            stream.start_stream()

            while stream.is_active():
                time.sleep(0.1)

            stream.stop_stream()
            stream.close()
            wf.close()

            p.terminate()
